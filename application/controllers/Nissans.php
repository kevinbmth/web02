<?php
    class Nissans extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Nissan');

      }
      //Funcion que renderiza la vista index
      public function index(){
        $data['nissan']=$this->Nissan->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('nissans/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('nissans/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoNissan=array("placa_ni"=>$this->input->post('placa_ni'),
        "marca_ni"=>$this->input->post('marca_ni'),
        "color_ni"=>$this->input->post('color_ni'),
        "anio_ni"=>$this->input->post('anio_ni'),
        "tipo_ni"=>$this->input->post('tipo_ni'),
      );
        if($this->Nissan->insertar($datosNuevoNissan)){
          redirect('nissans/index');

        }else{
          echo "<h1>ERROR DE LA PAGINA</h1>";
        }

      }
      //funcion para eliminar instructores
      public function eliminar($placa_to){
        if ($this->Nissan->borrar($placa_to)) {
          redirect('nissans/index');
        }else {
          echo "error al borrar";
        }
      }
    } // Cierre de la clase
?>
