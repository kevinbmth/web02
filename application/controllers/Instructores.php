<?php
    class Instructores extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Instructor');

      }
      //Funcion que renderiza la vista index
      public function index(){
        $data['instructores']=$this->Instructor->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('instructores/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('instructores/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoInstructor=array("placa_to"=>$this->input->post('placa_to'),
        "marca_to"=>$this->input->post('marca_to'),
        "color_to"=>$this->input->post('color_to'),
        "anio_to"=>$this->input->post('anio_to'),
        "tipo_to"=>$this->input->post('tipo_to'),
      );
        if($this->Instructor->insertar($datosNuevoInstructor)){
          redirect('instructores/index');

        }else{
          echo "<h1>ERROR DE LA PAGINA</h1>";
        }

      }
      //funcion para eliminar instructores
      public function eliminar($placa_to){
        if ($this->Instructor->borrar($placa_to)) {
          redirect('instructores/index');
        }else {
          echo "error al borrar";
        }
      }
    } // Cierre de la clase
?>
