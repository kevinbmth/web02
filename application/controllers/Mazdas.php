<?php
    class Mazdas extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Mazda');

      }
      //Funcion que renderiza la vista index
      public function index(){
        $data['mazda']=$this->Mazda->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('mazdas/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('mazdas/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoMazda=array("placa_ma"=>$this->input->post('placa_ma'),
        "marca_ma"=>$this->input->post('marca_ma'),
        "color_ma"=>$this->input->post('color_ma'),
        "anio_ma"=>$this->input->post('anio_ma'),
        "tipo_ma"=>$this->input->post('tipo_ma'),
      );
        if($this->Mazda->insertar($datosNuevoMazda)){
          redirect('mazdas/index');

        }else{
          echo "<h1>ERROR DE LA PAGINA</h1>";
        }

      }
      //funcion para eliminar instructores
      public function eliminar($placa_ma){
        if ($this->Mazda->borrar($placa_ma)) {
          redirect('Mazdas/index');
        }else {
          echo "error al borrar";
        }
      }
    } // Cierre de la clase
?>
