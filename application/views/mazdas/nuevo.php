<h1>NUEVO AUTOMOVIL</h1>
<form class="" action="<?php echo site_url();?>/mazdas/guardar" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">PLACA:</label>
      <br>
      <input type="text"
      placeholder="PLACA DEL AUTOMOVIL"
      class="form-control" name="placa_ma" value="">
    </div>
    <div class="col-md-4">
      <label for="">MARCA:</label>
      <br>
      <input type="text"
      placeholder="MARCA DEL AUTOMOVIL"
      class="form-control" name="marca_ma" value="">
    </div>
    <div class="col-md-4">
      <label for="">COLOR:</label>
      <br>
      <input type="text"
      placeholder="COLOR DEL AUTOMOVIL"
      class="form-control" name="color_ma" value="">
    </div>
    <div class="col-md-4">
      <label for="">AÑO:</label>
      <br>
      <input type="text"
      placeholder="AÑO DE FABRICACION"
      class="form-control" name="anio_ma" value="">
    </div>
    <div class="col-md-4">
      <label for="">TIPO:</label>
      <br>
      <input type="text"
      placeholder="TIPO DE AUTOMOVIL"
      class="form-control" name="tipo_ma" value="">
    </div>
  </div>
  <br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;
    <a href="<?php echo site_url(); ?>/mazdas/index" class="btn btn-danger">CANCELAR</a>
  </div>
  <br>
</form
