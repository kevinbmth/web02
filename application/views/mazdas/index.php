<div class="row">
  <div class="col-md-8">
    <h1 class="center">LISTADO DE LOS VEHICULOS</h1>
  </div>
  <div class="col-md-4 text:rigth;">
    <a href="<?php echo site_url(); ?>/mazdas/nuevo" class="btn btn-primary"> <i class="glyphicon glyphicon-plus"></i> agregar vehiculo</a>

  </div>
</div>
<br>
<?php if ($mazda) : ?>
    <table class="table table=striped table-bordered table-hover">
      <thead>
        <tr>
            <th>PLACA</th>
            <th>MARCA</th>
            <th>COLOR</th>
            <th>AÑO</th>
            <th>TIPO</th>
            <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
            <?php foreach ($mazda
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->placa_ma; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal-> marca_ma; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->color_ma; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->anio_ma; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal-> tipo_ma; ?>
                  </td>

                  <td class="text-center">
                    <a href="#" title="Editar auto">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url(); ?>/mazdas/eliminar/<?php echo $filaTemporal->placa_ma; ?>"
                    title="Eliminar auto" onclick="return confirm('ESTA SEGURO QUE DESEA ELIMINAR EL ELEMENTO SELECCIONADO?')" style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>

              </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have Instructores<h1>
        <?php endif; ?>
