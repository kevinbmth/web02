<h1>NUEVO AUTOMOVIL</h1>
<form class="" action="<?php echo site_url();?>/nissans/guardar" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">PLACA:</label>
      <br>
      <input type="text"
      placeholder="PLACA DEL AUTOMOVIL"
      class="form-control" name="placa_ni" value="">
    </div>
    <div class="col-md-4">
      <label for="">MARCA:</label>
      <br>
      <input type="text"
      placeholder="MARCA DEL AUTOMOVIL"
      class="form-control" name="marca_ni" value="">
    </div>
    <div class="col-md-4">
      <label for="">COLOR:</label>
      <br>
      <input type="text"
      placeholder="COLOR DEL AUTOMOVIL"
      class="form-control" name="color_ni" value="">
    </div>
    <div class="col-md-4">
      <label for="">AÑO:</label>
      <br>
      <input type="text"
      placeholder="AÑO DE FABRICACION"
      class="form-control" name="anio_ni" value="">
    </div>
    <div class="col-md-4">
      <label for="">TIPO:</label>
      <br>
      <input type="text"
      placeholder="TIPO DE AUTOMOVIL"
      class="form-control" name="tipo_ni" value="">
    </div>
  </div>
  <br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;
    <a href="<?php echo site_url(); ?>/nissans/index" class="btn btn-danger">CANCELAR</a>
  </div>
  <br>
</form
