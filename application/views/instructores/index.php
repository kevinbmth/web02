<div class="row">
  <div class="col-md-8">
    <h1 class="center">LISTADO DE LOS VEHICULOS</h1>
  </div>
  <div class="col-md-4 text:rigth;">
    <a href="<?php echo site_url(); ?>/instructores/nuevo" class="btn btn-primary"> <i class="glyphicon glyphicon-plus"></i> agregar vehiculo</a>

  </div>
</div>
<br>
<?php if ($instructores) : ?>
    <table class="table table=striped table-bordered table-hover">
      <thead>
        <tr>
            <th>PLACA</th>
            <th>MARCA</th>
            <th>COLOR</th>
            <th>AÑO</th>
            <th>TIPO</th>
            <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
            <?php foreach ($instructores
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->placa_to; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal-> marca_to; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->color_to; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->anio_to; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal-> tipo_to; ?>
                  </td>

                  <td class="text-center">
                    <a href="#" title="Editar Instructor">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url(); ?>/instructores/eliminar/<?php echo $filaTemporal->placa_to; ?>"
                    title="Eliminar Instructor" onclick="return confirm('ESTA SEGURO QUE DESEA ELIMINAR EL ELEMENTO SELECCIONADO?')" style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>

              </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have VEHICULOS<h1>
        <?php endif; ?>
