<?php

    class Nissan extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un instructor
        function insertar($datos){
            return $this->db->insert("nissan", $datos);

        }
        //Funcion para consultar Instructores
        function obtenerTodos(){
            $listadoNissan=
            $this->db->get("nissan");

            if($listadoNissan
                ->num_rows()>0){//Si hay datos
                    return $listadoNissan->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar Instructores
        function borrar($placa_ni){
            $this->db->where("placa_ni",$placa_ni);
            return $this->db->delete("nissan");

        }

    }//Cierre de la clase


?>
