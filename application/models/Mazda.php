<?php

    class Mazda extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un instructor
        function insertar($datos){
            return $this->db->insert("mazda", $datos);

        }
        //Funcion para consultar Instructores
        function obtenerTodos(){
            $listadoMazda=
            $this->db->get("mazda");

            if($listadoMazda
                ->num_rows()>0){//Si hay datos
                    return $listadoMazda->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar Instructores
        function borrar($placa_ma){
            $this->db->where("placa_ma",$placa_ma);
            return $this->db->delete("mazda");

        }

    }//Cierre de la clase


?>
