<?php

    class Instructor extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un instructor
        function insertar($datos){
            return $this->db->insert("toyota", $datos);

        }
        //Funcion para consultar Instructores
        function obtenerTodos(){
            $listadoInstructores=
            $this->db->get("toyota");

            if($listadoInstructores
                ->num_rows()>0){//Si hay datos
                    return $listadoInstructores->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar Instructores
        function borrar($placa_to){
            $this->db->where("placa_to",$placa_to);
            return $this->db->delete("toyota");

        }

    }//Cierre de la clase


?>
